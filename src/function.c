

#include "function.h"

#include <stdlib.h>

hxnoise_function *
hxnoise_function_new(hxnoise_function_type type)
{
    hxnoise_function *self;
    if (!(self = malloc(sizeof(hxnoise_function)))) {
        return NULL;
    }

    self->type = type;
    self->name = NULL;
    self->input = NULL;
    self->args = NULL;
    self->value = 0;

    return self;
}

void
hxnoise_function_destroy(hxnoise_function *self)
{
    if (self->input)
        hxnoise_function_destroy(self->input);

    hxnoise_function_list *i = self->args;
    hxnoise_function_list *j;
    while (i != NULL) {
        hxnoise_function_destroy(i->elem);
        j = i;
        i = i->next;
        free(j);
    }

    if (self->name)
        free(self->name);

    free(self);
}
