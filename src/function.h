
#ifndef HXNOISE_FUNCTION_H
#define HXNOISE_FUNCTION_H 1

typedef enum {
    HN_FUNC,
    HN_CONST_VALUE,
    HN_CONST_STRING,
    HN_CONST_BOOL,
    HN_GLOBAL,
    HN_LAMBDA,
    HN_EXTERNAL
} hxnoise_function_type;

struct hxnoise_function_list;

typedef struct hxnoise_function {
    hxnoise_function_type type;
    char *name;
    struct hxnoise_function *input;
    struct hxnoise_function_list *args;
    double value;
} hxnoise_function;

typedef struct hxnoise_function_list {
    struct hxnoise_function *elem;
    struct hxnoise_function_list *next;
} hxnoise_function_list;

/* ------------------------------------------------------------------ */

hxnoise_function *
hxnoise_function_new(hxnoise_function_type type);

void
hxnoise_function_destroy(hxnoise_function *self);

#endif
