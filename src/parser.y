%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "function.h"
#include "parser.tab.h"
#include "lex.yy.h"

void yyerror(yyscan_t scanner, struct hxnoise_function** result, const char* msg) 
{
    printf("%s\n", msg);
}

%}

%code requires {

#ifndef YY_TYPEDEF_YY_SCANNER_T
#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;
#endif

}

%define api.pure
%define parse.error verbose
%lex-param { yyscan_t scanner }
%parse-param { yyscan_t scanner }
%parse-param { struct hxnoise_function** result }

%union {
    struct hxnoise_function* func;
    struct hxnoise_function_list* param_list;
    char* string;
    double value;
    int   tok;
}

%token <tok>         DOLLAR AT LPAREN RPAREN LACCOL RACCOL COMMA
%token <string>      IDENTIFIER STRING
%token <value>       VALUE
%type  <func>        function 
%type  <param_list>  param_list

%left COLON

%start input

%%

input: function {*result = $1; }
        ;

function : VALUE 
                { $$ = hxnoise_function_new(HN_CONST_VALUE); $$->value = $1; }
         | STRING
                { $$ = hxnoise_function_new(HN_CONST_STRING); $$->name = $1; }
         | DOLLAR IDENTIFIER
                { $$ = hxnoise_function_new(HN_GLOBAL); $$->name = $2; }
         | AT IDENTIFIER
                { $$ = hxnoise_function_new(HN_EXTERNAL); $$->name = $2; }
         | IDENTIFIER
                { $$ = hxnoise_function_new(HN_FUNC); $$->name = $1; }
         | IDENTIFIER LPAREN param_list RPAREN
                { $$ = hxnoise_function_new(HN_FUNC); $$->name = $1; $$->args = $3; }
         | function COLON function
                { $3->input = $1; $$ = $3; }
         | LACCOL function RACCOL
                { 
                  $$ = hxnoise_function_new(HN_LAMBDA); 
                  $$->args = malloc(sizeof(hxnoise_function_list));
                  $$->args->elem = $2;
                  $$->args->next = NULL;
                }
         ;

param_list : function
                { 
                    $$ = malloc(sizeof(hxnoise_function_list)); 
                    $$->elem = $1;
                    $$->next = NULL;
                }
           | function COMMA param_list
                {
                    $$ = malloc(sizeof(hxnoise_function_list)); 
                    $$->elem = $1;
                    $$->next = $3;
                }
           ;

%%
