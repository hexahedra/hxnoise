
#ifndef SLIST_H
#define SLIST_H

#include <stdlib.h>

typedef struct slist_node {
    struct slist_node *next;
    void *val;
} slist_node_t;

typedef struct {
    slist_node_t *head;
    slist_node_t *tail;
} slist_t;


slist_node_t *
slist_node_new(void *val);

void
slist_node_destroy(slist_node_t *node);

slist_t *
slist_new();

void
slist_destroy(slist_t *list);

slist_node_t *
slist_lpush(slist_t *list, slist_node_t *node);

slist_node_t *
slist_rpush(slist_t *list, slist_node_t *node);

void *
slist_lpop(slist_t *list);

int
slist_len(slist_t *list);

void
slist_foreach(slist_t *list, void (*callback)(void *));

void
slist_splice(slist_t *list, slist_t *append);

#endif
