
%{
#include <string.h>
#include "parser.tab.h"
%}

%option reentrant never-interactive noyywrap nounput
%option bison-bridge
%option header-file="lex.yy.h"

%x COMMENT    

%%


"/*"                { BEGIN(COMMENT); }
<COMMENT>"*/"       { BEGIN(INITIAL); }
<COMMENT>.          { }

[ \t\r\n]+          /* Skip whitespace */
[a-z_]+             { yylval->string = strdup(yytext); return IDENTIFIER; }
[-]?[0-9]+\.?[0-9]* { yylval->value = atof(yytext); return (VALUE); }
\"[a-zA-Z0-9/_.]*\" { yylval->string = strdup(yytext); return (STRING); }

":"                 return (COLON);
","                 return (COMMA);
"("                 return (LPAREN);
")"                 return (RPAREN);
"{"                 return (LACCOL);
"}"                 return (RACCOL);
"$"                 return (DOLLAR);
"@"                 return (AT);

.                   yyterminate();
