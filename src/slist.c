
#include "slist.h"

slist_node_t *
slist_node_new(void *val)
{
    slist_node_t *n = malloc(sizeof(slist_node_t));

    if (n != NULL) {
        n->next = NULL;
        n->val = val;
    }

    return n;
}

void
slist_node_destroy(slist_node_t *node)
{
    free(node->val);
    free(node);
}

slist_t *
slist_new()
{
    slist_t *n = malloc(sizeof(slist_t));

    if (n != NULL) {
        n->head = NULL;
        n->tail = NULL;
    }

    return n;
}

void
slist_destroy(slist_t *list)
{
    slist_node_t *i = list->head;
    while (i != NULL) {
        slist_node_t *n = i->next;
        slist_node_destroy(i);
        i = n;
    }

    free(list);
}

slist_node_t *
slist_lpush(slist_t *list, slist_node_t *node)
{
    node->next = list->head;
    list->head = node;

    if (list->tail == NULL) {
        list->tail = node;
    }

    return node;
}

slist_node_t *
slist_rpush(slist_t *list, slist_node_t *node)
{
    if (list->tail == NULL) {
        return slist_lpush(list, node);
    }

    list->tail->next = node;
    node->next = NULL;
    list->tail = node;

    return node;
}

void *
slist_lpop(slist_t *list)
{
    void *result;
    slist_node_t *elem = list->head;

    if (elem == NULL) {
        return NULL;
    }

    result = elem->val;
    list->head = elem->next;
    free(elem);

    if (list->head == NULL) {
        list->tail = NULL;
    }

    return result;
}

int
slist_len(slist_t *list)
{
    int count = 0;
    slist_node_t *i = list->head;

    while (i != NULL) {
        i = i->next;
        count++;
    }

    return count;
}

void
slist_foreach(slist_t *list, void (*callback)(void *))
{
    slist_node_t *i = list->head;

    while (i != NULL) {
        callback(i->val);
        i = i->next;
    }
}

void
slist_splice(slist_t *list, slist_t *append)
{
    if (list->head == NULL) {
        list->head = append->head;
        list->tail = append->tail;
    } else {
        list->tail->next = append->head;
        list->tail = append->tail;
    }

    append->head = NULL;
    append->tail = NULL;
}
