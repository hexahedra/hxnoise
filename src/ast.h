
#ifndef HXNOISE_AST_H
#define HXNOISE_AST_H 1

#include "generator_context.h"
#include "function.h"


typedef enum {
    HNT_NONE,
    HNT_VAR,
    HNT_XY,
    HNT_XYZ,
    HNT_STRING,
    HNT_BOOL,
    HNT_EXTERN
} hxnoise_var_t;

typedef enum {
    HNN_ERROR,
    HNN_INPUT,
    HNN_CONST_VAR,
    HNN_CONST_STRING,
    HNN_CONST_BOOL,

    HNF_ROTATE2,
    HNF_SCALE2,
    HNF_SHIFT2,
    HNF_SWAP,
    HNF_MAP2,
    HNF_TURBULENCE2,

    HNF_ANGLE,
    HNF_CHEBYSHEV2,
    HNF_CHECKERBOARD2,
    HNF_DISTANCE2,
    HNF_EXTERNAL,
    HNF_LAMBDA,
    HNF_MANHATTAN2,
    HNF_FRACTAL2,
    HNF_PERLIN2,
    HNF_PNG_LOOKUP,
    HNF_SIMPLEX2,
    HNF_OPENSIMPLEX2,
    HNF_VORONOI2,
    HNF_WORLEY2,
    HNF_X,
    HNF_Y,

    HNF_ABS,
    HNF_ADD,
    HNF_BLEND,
    HNF_COS,
    HNF_CURVE_SPLINE,
    HNF_CURVE_LINEAR,
    HNF_DIV,
    HNF_MAX,
    HNF_MIN,
    HNF_MUL,
    HNF_NEG,
    HNF_POW,
    HNF_RANGE,
    HNF_ROUND,
    HNF_SAW,
    HNF_SIN,
    HNF_SQRT,
    HNF_SUB,
    HNF_TAN,

    HNF_AND,
    HNF_NOT,
    HNF_OR,
    HNF_XOR,

    HNF_ISEQ,
    HNF_GT,
    HNF_GTE,
    HNF_LT,
    HNF_LTE,

    HNF_THEN_ELSE,

    HNF_ROTATE3,
    HNF_SCALE3,
    HNF_SHIFT3,
    HNF_MAP3,
    HNF_TURBULENCE3,

    HNF_XPLANE,
    HNF_YPLANE,
    HNF_ZPLANE,

    HNF_XY,

    HNF_CHEBYSHEV3,
    HNF_CHECKERBOARD3,
    HNF_DISTANCE3,
    HNF_MANHATTAN3,
    HNF_FRACTAL3,
    HNF_PERLIN3,
    HNF_SIMPLEX3,
    HNF_OPENSIMPLEX3,
    HNF_WORLEY3,
    HNF_Z,

    HNF_NR_OF_FUNCTIONS
} hxnoise_func_t;

typedef struct hxnoise_ast_node_list hxnoise_ast_node_list;

typedef struct hxnoise_ast_node {
    hxnoise_func_t type;
    struct hxnoise_ast_node_list *inputs;
    hxnoise_var_t return_type;
    char *err_msg;
    int is_const;
    char *aux_string;
    double aux_var;
    int aux_bool;
} hxnoise_ast_node;

struct hxnoise_ast_node_list {
    struct hxnoise_ast_node *node;
    struct hxnoise_ast_node_list *next;
};

/* ---------------------------------------------------------------------- */

hxnoise_ast_node *
hxnoise_build_ast(hxnoise_function *func, hxnoise_generator_context *ctx);

void
hxnoise_ast_destroy(hxnoise_ast_node *node);

const char *
hxnoise_func_name(hxnoise_func_t id);

#endif

