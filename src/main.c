
#include <stdio.h>

#include "function.h"
#include "parser.tab.h"
#include "lex.yy.h"
#include "ast.h"
#include "compile.h"

void print(int depth, hxnoise_ast_node *ast);

int main(void)
{
    yyscan_t scanner;
    YY_BUFFER_STATE state;
    hxnoise_function *result = NULL;

    if (yylex_init(&scanner)) {
        printf("yylex init failed");
        return -1;
    }

    state = yy_scan_string("scale(40):perlin", scanner);
    if (yyparse(scanner, &result)) {
        printf ("yyparse failed\n");
        return -2;
    }

    yy_delete_buffer(state, scanner);
    yylex_destroy(scanner);

    function_in_t fn;
    fn.name = "my_func";
    fn.ast = hxnoise_build_ast(result, NULL);
    fn.precision = HNP_DOUBLE;

    char *res = hxnoise_compile(&fn, 1);
    
    puts(res);

    return 0;
}
