
#ifndef HXNOISE_OPENCL_FUNCTION_H
#define HXNOISE_OPENCL_FUNCTION_H 1

#define HXOCL_HEADER \
"#ifdef cl_khr_fp64\n"\
"  #pragma OPENCL EXTENSION cl_khr_fp64 : enable\n"\
"#elif defined(cl_amd_fp64)\n"\
"  #pragma OPENCL EXTENSION cl_amd_fp64 : enable\n"\
"#else\n"\
"  #error \"Double precision floating point not supported by OpenCL implementation.\"\n"\
"#endif\n\n"

#define HXOCL_KERNEL_3D_DOUBLE \
"kernel void %s(__global double* output,\n"\
"    double startx, double starty, double startz,\n"\
"    double stepx,  double stepy,  double stepz)\n"\
"{\n"\
"    int3 coord = (int3)(get_global_id(0), get_global_id(1), get_global_id(2));\n"\
"    int sizex = get_global_size(0);\n"\
"    int sizey = get_global_size(1);\n"\
"    double3 p = mad((double3)(stepx, stepy, stepz),\n"\
"                   (double3)(coord.x, coord.y, coord.z),\n"\
"                   (double3)(startx, starty, startz));\n"\
"    output[coord.z * sizex * sizey + coord.y * sizex + coord.x] = "


#define HXOCL_BLENDFUNCS \
"inline double blend3 (const double a)\n"\
"{\n"\
"    return a * a * (3.0 - 2.0 * a);\n"\
"}\n"\
"\n"\
"inline double blend5 (const double a)\n"\
"{\n"\
"    return a * a * a * (a * (a * 6.0 - 15.0) + 10.0);\n"\
"}\n"

#define HXOCL_LERP \
"inline double lerp (double x, double a, double b)\n"\
"{\n"\
"    return mad(x, b - a, a);\n"\
"}\n"\
"\n"\
"inline double2 lerp2d (const double x, const double2 a, const double2 b)\n"\
"{\n"\
"    return mad(x, b - a, a);\n"\
"}\n"\
"\n"\
"inline double4 lerp4d (const double x, const double4 a, const double4 b)\n"\
"{\n"\
"    return mad(x, b - a, a);\n"\
"}\n"

#define HXOCL_RNG_HASH \
"inline uint hash (int x, int y)\n"\
"{\n"\
"    return ((uint)x * 2120969693) ^ ((uint)y * 915488749) ^ ((uint)(x + 1103515245) * (uint)(y + 1234567));\n"\
"}\n"\
"\n"\
"inline uint hash3 (int x, int y, int z)\n"\
"{\n"\
"    return ((uint)x * 2120969693)\n"\
"            ^ ((uint)y * 915488749)\n"\
"            ^ ((uint)z * 22695477)\n"\
"            ^ ((uint)(x + 1103515245) * (uint)(y + 1234567) * (uint)(z + 134775813));\n"\
"}\n"\
"\n"\
"inline uint rng (uint last)\n"\
"{\n"\
"    return (1103515245 * last + 12345) & 0x7FFFFFFF;\n"\
"}\n"

#define HXOCL_PERLIN_GRADIENT \
"#define ONE_F1   (1.0f)\n"\
"#define ZERO_F1  (0.0f)\n"\
"\n"\
"__constant int P_MASK = 255;\n"\
"__constant int P_SIZE = 256;\n"\
"__constant int P[512] = {151,160,137,91,90,15,\n"\
"  131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,\n"\
"  190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,\n"\
"  88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,\n"\
"  77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,\n"\
"  102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,\n"\
"  135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,\n"\
"  5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,\n"\
"  223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,\n"\
"  129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,\n"\
"  251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,\n"\
"  49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,\n"\
"  138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,\n"\
"  151,160,137,91,90,15,\n"\
"  131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,\n"\
"  190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,\n"\
"  88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,\n"\
"  77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,\n"\
"  102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,\n"\
"  135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,\n"\
"  5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,\n"\
"  223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,\n"\
"  129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,\n"\
"  251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,\n"\
"  49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,\n"\
"  138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,\n"\
"  };\n"\
"\n"\
"__constant int G_MASK = 15;\n"\
"__constant int G_SIZE = 16;\n"\
"__constant int G_VECSIZE = 4;\n"\
"__constant float G[16*4] = {\n"\
"      +ONE_F1,  +ONE_F1, +ZERO_F1, +ZERO_F1,\n"\
"      -ONE_F1,  +ONE_F1, +ZERO_F1, +ZERO_F1,\n"\
"      +ONE_F1,  -ONE_F1, +ZERO_F1, +ZERO_F1,\n"\
"      -ONE_F1,  -ONE_F1, +ZERO_F1, +ZERO_F1,\n"\
"      +ONE_F1, +ZERO_F1,  +ONE_F1, +ZERO_F1,\n"\
"      -ONE_F1, +ZERO_F1,  +ONE_F1, +ZERO_F1,\n"\
"      +ONE_F1, +ZERO_F1,  -ONE_F1, +ZERO_F1,\n"\
"      -ONE_F1, +ZERO_F1,  -ONE_F1, +ZERO_F1,\n"\
"     +ZERO_F1,  +ONE_F1,  +ONE_F1, +ZERO_F1,\n"\
"     +ZERO_F1,  -ONE_F1,  +ONE_F1, +ZERO_F1,\n"\
"     +ZERO_F1,  +ONE_F1,  -ONE_F1, +ZERO_F1,\n"\
"     +ZERO_F1,  -ONE_F1,  -ONE_F1, +ZERO_F1,\n"\
"      +ONE_F1,  +ONE_F1, +ZERO_F1, +ZERO_F1,\n"\
"      -ONE_F1,  +ONE_F1, +ZERO_F1, +ZERO_F1,\n"\
"     +ZERO_F1,  -ONE_F1,  +ONE_F1, +ZERO_F1,\n"\
"     +ZERO_F1,  -ONE_F1,  -ONE_F1, +ZERO_F1\n"\
"};\n"

#define HXOCL_PERLIN2 \
"inline double gradient_noise2d (double2 xy, int2 ixy, uint seed)\n"\
"{\n"\
"    ixy.x += seed * 1013;\n"\
"    ixy.y += seed * 1619;\n"\
"    ixy &= P_MASK;\n\n"\
"    int index = (P[ixy.x+P[ixy.y]] & G_MASK) * G_VECSIZE;\n"\
"    double2 g = (double2)(G[index], G[index+1]);\n\n"\
"    return dot(xy, g);\n"\
"}\n\n"\
"double p_perlin (double2 xy, uint seed)\n"\
"{\n"\
"    double2 t = floor(xy);\n"\
"    int2 xy0 = (int2)((int)t.x, (int)t.y);\n"\
"    double2 xyf = xy - t;\n"\
"\n"\
"    const int2 I01 = (int2)(0, 1);\n"\
"    const int2 I10 = (int2)(1, 0);\n"\
"    const int2 I11 = (int2)(1, 1);\n"\
"\n"\
"    const double2 F01 = (double2)(0.0, 1.0);\n"\
"    const double2 F10 = (double2)(1.0, 0.0);\n"\
"    const double2 F11 = (double2)(1.0, 1.0);\n"\
"\n"\
"    const double n00 = gradient_noise2d(xyf      , xy0, seed);\n"\
"    const double n10 = gradient_noise2d(xyf - F10, xy0 + I10, seed);\n"\
"    const double n01 = gradient_noise2d(xyf - F01, xy0 + I01, seed);\n"\
"    const double n11 = gradient_noise2d(xyf - F11, xy0 + I11, seed);\n"\
"\n"\
"    const double2 n0001 = (double2)(n00, n01);\n"\
"    const double2 n1011 = (double2)(n10, n11);\n"\
"    const double2 n2 = lerp2d(blend5(xyf.x), n0001, n1011);\n"\
"\n"\
"    return lerp(blend5(xyf.y), n2.x, n2.y) * 1.227;\n"\
"}\n"

#define HXOCL_PERLIN3 \
"inline double gradient_noise3d (int3 ixyz, double3 xyz, uint seed)\n"\
"{\n"\
"    ixyz.x += seed * 1013;\n"\
"    ixyz.y += seed * 1619;\n"\
"    ixyz.z += seed * 997;\n"\
"    ixyz &= P_MASK;\n"\
"\n"\
"    int index = (P[ixyz.x+P[ixyz.y+P[ixyz.z]]] & G_MASK) * G_VECSIZE;\n"\
"    double3 g = (double3)(G[index], G[index+1], G[index+2]);\n"\
"\n"\
"    return dot(xyz, g);\n"\
"}\n"\
"\n"\
"double p_perlin3 (double3 xyz, uint seed)\n"\
"{\n"\
"    double3 t = floor(xyz);\n"\
"    int3 xyz0 = (int3)((int)t.x, (int)t.y, (int)t.z);\n"\
"    double3 xyzf = xyz - t;\n"\
"\n"\
"    const int3 I001 = (int3)(0, 0, 1);\n"\
"    const int3 I010 = (int3)(0, 1, 0);\n"\
"    const int3 I011 = (int3)(0, 1, 1);\n"\
"    const int3 I100 = (int3)(1, 0, 0);\n"\
"    const int3 I101 = (int3)(1, 0, 1);\n"\
"    const int3 I110 = (int3)(1, 1, 0);\n"\
"    const int3 I111 = (int3)(1, 1, 1);\n"\
"\n"\
"    const double3 F001 = (double3)(0.0, 0.0, 1.0);\n"\
"    const double3 F010 = (double3)(0.0, 1.0, 0.0);\n"\
"    const double3 F011 = (double3)(0.0, 1.0, 1.0);\n"\
"    const double3 F100 = (double3)(1.0, 0.0, 0.0);\n"\
"    const double3 F101 = (double3)(1.0, 0.0, 1.0);\n"\
"    const double3 F110 = (double3)(1.0, 1.0, 0.0);\n"\
"    const double3 F111 = (double3)(1.0, 1.0, 1.0);\n"\
"\n"\
"    const double n000 = gradient_noise3d(xyz0       , xyzf       , seed);\n"\
"    const double n001 = gradient_noise3d(xyz0 + I001, xyzf - F001, seed);\n"\
"    const double n010 = gradient_noise3d(xyz0 + I010, xyzf - F010, seed);\n"\
"    const double n011 = gradient_noise3d(xyz0 + I011, xyzf - F011, seed);\n"\
"    const double n100 = gradient_noise3d(xyz0 + I100, xyzf - F100, seed);\n"\
"    const double n101 = gradient_noise3d(xyz0 + I101, xyzf - F101, seed);\n"\
"    const double n110 = gradient_noise3d(xyz0 + I110, xyzf - F110, seed);\n"\
"    const double n111 = gradient_noise3d(xyz0 + I111, xyzf - F111, seed);\n"\
"\n"\
"    double4 n40 = (double4)(n000, n001, n010, n011);\n"\
"    double4 n41 = (double4)(n100, n101, n110, n111);\n"\
"\n"\
"    double4 n4 = lerp4d(blend5(xyzf.x), n40, n41);\n"\
"    double2 n2 = lerp2d(blend5(xyzf.y), n4.xy, n4.zw);\n"\
"    double n = lerp(blend5(xyzf.z), n2.x, n2.y);\n"\
"\n"\
"    return n * 1.216;\n"\
"}\n"

#define HXOCL_WORLEY2 \
"double2 p_worley (const double2 p, uint seed)\n"\
"{\n"\
"    double2 t = floor(p);\n"\
"    int2 xy0 = (int2)((int)t.x, (int)t.y);\n"\
"    double2 xyf = p - t;\n"\
"\n"\
"    double f0 = 9999.9;\n"\
"    double f1 = 9999.9;\n"\
"\n"\
"    for (int i = -1; i < 2; ++i)\n"\
"    {\n"\
"        for (int j = -1; j < 2; ++j)\n"\
"        {\n"\
"            int2 square = xy0 + (int2)(i,j);\n"\
"            uint h = rng(hash(square.x + seed, square.y));\n"\
"\n"\
"            double2 rnd_pt;\n"\
"            rnd_pt.x = (double)i + ((double)(h & 0xFFFF) / (double)0x10000);\n"\
"            h = rng(h);\n"\
"            rnd_pt.y = (double)j + ((double)(h & 0xFFFF) / (double)0x10000);\n"\
"\n"\
"            double dist = distance(xyf, rnd_pt);\n"\
"            if (dist < f0)\n"\
"            {\n"\
"                f1 = f0;\n"\
"                f0 = dist;\n"\
"            }\n"\
"            else if (dist < f1)\n"\
"            {\n"\
"                f1 = dist;\n"\
"            }\n"\
"        }\n"\
"    }\n"\
"    return (double2)(f0, f1);\n"\
"}\n"

#define HXOCL_ROTATE2 \
"inline double2 p_rotate (double2 p, double a)\n"\
"{\n"\
"    double t = a * M_PI;\n"\
"    return (double2)(p.x * cos(t) - p.y * sin(t), p.x * sin(t) + p.y * cos(t));\n"\
"}\n"

#define HXOCL_ROTATE3 \
"inline double16 rotMatrix(double3 a, double angle)\n"\
"{\n"\
"    const double u2 = a.x * a.x;\n"\
"    const double v2 = a.y * a.y;\n"\
"    const double w2 = a.z * a.z;\n"\
"    const double l = u2 + v2 + w2;\n"\
"    const double sl = sqrt(l);\n"\
"    const double sa = sin(angle);\n"\
"    const double ca = cos(angle);\n"\
"\n"\
"    return (double16)(\n"\
"\n"\
"    (u2 + (v2 + w2) * ca) / l,\n"\
"    (a.x * a.y * (1.0 - ca) - a.z * sl * sa) / l,\n"\
"    (a.x * a.z * (1.0 - ca) + a.y * sl * sa) / l,\n"\
"    0.0,\n"\
"\n"\
"    (a.x * a.y * (1.0 - ca) + a.z * sl * sa) / l,\n"\
"    (v2 + (u2 + w2) * ca) / l,\n"\
"    (a.y * a.z * (1.0 - ca) - a.x * sl * sa) / l,\n"\
"    0.0,\n"\
"\n"\
"    (a.x * a.z * (1.0 - ca) - a.y * sl * sa) / l,\n"\
"    (a.y * a.z * (1.0 - ca) + a.x * sl * sa) / l,\n"\
"    (w2 + (u2 + v2) * ca) / l,\n"\
"    0.0,\n"\
"\n"\
"    0.0, 0.0, 0.0, 1.0\n"\
"    );\n"\
"}\n"\
"\n"\
"inline double3 p_rotate3 (double3 p, double3 axis, double a)\n"\
"{\n"\
"    double4 h = (double4)(p, 1);\n"\
"    double16 m = rotMatrix(axis, a * M_PI);\n"\
"    return (double3)(\n"\
"                dot(m.s048C, h),\n"\
"                dot(m.s159D, h),\n"\
"                dot(m.s26AE, h)\n"\
"                );\n"\
"}\n"

#define HXOCL_SWAP \
"inline double2 p_swap (double2 p)\n"\
"{\n"\
"    return (double2)(p.y, p.x);\n"\
"}\n"

#define HXOCL_ANGLE \
"inline double p_angle (double2 p)\n"\
"{\n"\
"    return atan2pi(p.y, p.x);\n"\
"}\n"

#define HXOCL_CHEBYSHEV2 \
"inline double p_chebyshev (double2 p)\n"\
"{\n"\
"    return fmax(fabs(p.x), fabs(p.y));\n"\
"}\n"

#define HXOCL_CHEBYSHEV3 \
"inline double p_chebyshev3 (double3 p)\n"\
"{\n"\
"    return fmax(fmax(fabs(p.x), fabs(p.y)), fabs(p.z));\n"\
"}\n"

#define HXOCL_SAW \
"inline double p_saw (double n)\n"\
"{\n"\
"    return n - floor(n);\n"\
"}\n"

#define HXOCL_CHECKERBOARD2 \
"inline double p_checkerboard (double2 p)\n"\
"{\n"\
"    double2 sp = p - floor(p);\n"\
"    return (sp.x < 0.5) ^ (sp.y < 0.5) ? 1.0 : -1.0;\n"\
"}\n"

#define HXOCL_CHECKERBOARD3 \
"inline double p_checkerboard3 (double3 p)\n"\
"{\n"\
"    double3 sp = p - floor(p);\n"\
"    return (sp.x < 0.5) ^ (sp.y < 0.5) ^ (sp.z < 0.5) ? 1.0 : -1.0;\n"\
"}\n"

#define HXOCL_MANHATTAN2 \
"inline double p_manhattan (double2 p)\n"\
"{\n"\
"    return fabs(p.x) + fabs(p.y);\n"\
"}\n"

#define HXOCL_MANHATTAN3 \
"inline double p_manhattan3 (double3 p)\n"\
"{\n"\
"    return fabs(p.x) + fabs(p.y) + fabs(p.z);\n"\
"}\n"

#define HXOCL_BLEND \
"inline double p_blend (double x, double a, double b)\n"\
"{\n"\
"    x = (clamp(x, -1.0, 1.0) + 1.0) / 2.0;\n"\
"    return lerp(x, a, b);\n"\
"}\n"

#define HXOCL_RANGE \
"inline double p_range (double x, double a, double b)\n"\
"{\n"\
"    return clamp((a * 0.5 + x + 1.0) * ((b - a) * 0.5), a, b);\n"\
"}\n"

#define HXOCL_IS_IN_CIRCLE \
"inline bool p_is_in_circle (double2 p, double r)\n"\
"{\n"\
"    return length(p) <= r;\n"\
"}\n"

#define HXOCL_IS_IN_RECTANGLE \
"inline bool p_is_in_rectangle (double2 p, double x1, double y1, double x2, double y2)\n"\
"{\n"\
"    return p.x >= x1 && p.y >= y1 && p.x <= x2 && p.y <= y2;\n"\
"}\n"

#endif
