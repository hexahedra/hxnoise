
#include "compile.h"

#include <stdlib.h>
#include <string.h>

#include "buffer.h"
#include "opencl_functions.h"
#include "slist.h"

typedef struct function_def {
    char *name;
    buffer_t *body;
} function_def_t;

typedef struct context {
    int count;
    buffer_t *buf;
    slist_t *funcs;
    char used_funcs[HNF_NR_OF_FUNCTIONS];
} context_t;

static void
pl(hxnoise_ast_node *n, context_t *ctx);

static void
co(hxnoise_ast_node *n, context_t *ctx);

static const char *simple_func(hxnoise_func_t t)
{
    switch (t) {
    case HNF_SWAP:
        return "p_swap";
    case HNF_ANGLE:
        return "p_angle";
    case HNF_CHEBYSHEV2:
        return "p_chebyshev";
    case HNF_CHEBYSHEV3:
        return "p_chebyshev3";
    case HNF_CHECKERBOARD2:
        return "p_checkerboard";
    case HNF_CHECKERBOARD3:
        return "p_checkerboard";
    case HNF_DISTANCE2:
        return "length";
    case HNF_DISTANCE3:
        return "length";
    case HNF_MANHATTAN2:
        return "p_manhattan";
    case HNF_MANHATTAN3:
        return "p_manhattan3";
    case HNF_PERLIN2:
        return "p_perlin";
    case HNF_PERLIN3:
        return "p_perlin3";
    case HNF_ABS:
        return "fabs";
    case HNF_COS:
        return "cospi";
    case HNF_SIN:
        return "sinpi";
    case HNF_TAN:
        return "tanpi";
    case HNF_MIN:
        return "fmin";
    case HNF_MAX:
        return "fmax";
    case HNF_ROUND:
        return "fround";
    case HNF_SQRT:
        return "sqrt";
    default:
        return NULL;
    }
}

static const char *opencl_impl(hxnoise_func_t t)
{
    switch (t) {
        case HNF_PERLIN2: return HXOCL_PERLIN2;
        case HNF_PERLIN3: return HXOCL_PERLIN3;
        case HNF_WORLEY2: return HXOCL_WORLEY2;
        case HNF_ROTATE2: return HXOCL_ROTATE2;
        case HNF_ROTATE3: return HXOCL_ROTATE3;
        case HNF_SWAP: return HXOCL_SWAP;
        case HNF_ANGLE: return HXOCL_ANGLE;
        case HNF_CHEBYSHEV2: return HXOCL_CHEBYSHEV2;
        case HNF_CHEBYSHEV3: return HXOCL_CHEBYSHEV3;
        case HNF_SAW: return HXOCL_SAW;
        case HNF_CHECKERBOARD2: return HXOCL_CHECKERBOARD2;
        case HNF_CHECKERBOARD3: return HXOCL_CHECKERBOARD3;
        case HNF_MANHATTAN2: return HXOCL_MANHATTAN2;
        case HNF_MANHATTAN3: return HXOCL_MANHATTAN3;
        case HNF_BLEND: return HXOCL_BLEND;
        case HNF_RANGE: return HXOCL_RANGE;

        default: return NULL;
    }
}

static void _putd(context_t *ctx, double v)
{
    char tmp[32];
    int len = snprintf(tmp, 32, "%.10g", v);
    buffer_append_n(ctx->buf, tmp, len);
}

static void _putc(context_t *ctx, char c)
{
    buffer_append_n(ctx->buf, &c, 1);
}

static void _puts(context_t *ctx, const char *s)
{
    buffer_append(ctx->buf, s);
}

static void
infix(hxnoise_ast_node *n, const char *op, context_t *ctx)
{
    _putc(ctx, '(');
    co(n->inputs->node, ctx);
    _puts(ctx, op);
    co(n->inputs->next->node, ctx);
    _putc(ctx, ')');
}

static function_def_t *
mk_fn_def(const char *name, context_t *ctx)
{
    int len = strlen(name) + 8;
    char *buf = malloc(len);
    snprintf(buf, len, "%s%i", name, ++ctx->count);

    function_def_t *result = malloc(sizeof(function_def_t));
    result->name = buf;
    result->body = NULL;

    return result;
}

static void
map_fn(hxnoise_ast_node *n, context_t *ctx)
{
    function_def_t *fn = mk_fn_def("ip_map", ctx);
    buffer_t *buf = buffer_new();
    buffer_appendf(buf, "inline double2 %s(const double2 p) {\n    return (double2)(", fn->name);

    context_t ctx2;
    ctx2.count = ctx->count;
    ctx2.buf = buffer_new();
    ctx2.funcs = slist_new();;
    co(n->inputs->next->node, &ctx2);
    buffer_append_n(ctx2.buf, ",", 1);
    co(n->inputs->next->next->node, &ctx2);

    buffer_append(buf, ctx2.buf->data);
    buffer_free(ctx2.buf);
    buffer_append(buf, ");\n}\n\n");

    slist_splice(ctx->funcs, ctx2.funcs);
    slist_destroy(ctx2.funcs);

    fn->body = buf;
    slist_rpush(ctx->funcs, slist_node_new(fn));

    _puts(ctx, fn->name);
    _putc(ctx, '(');
    co(n->inputs->node, ctx);
    _putc(ctx, ')');
}

static void
co(hxnoise_ast_node *n, context_t *ctx)
{
    ctx->used_funcs[n->type] = 1;

    const char *sf = simple_func(n->type);
    if (sf != NULL) {
        _puts(ctx, sf);
        pl(n, ctx);
        return;
    }

    switch (n->type) {
    case HNN_ERROR:
        _puts(ctx, ">>> UNKNOWN FUNCTION <<<");
        break;

    case HNN_INPUT:
        _putc(ctx, 'p');
        break;

    case HNN_CONST_VAR:
        _putd(ctx, n->aux_var);
        break;

    case HNN_CONST_STRING:
        _puts(ctx, ">>> STRING <<<");
        break;

    case HNN_CONST_BOOL:
        if (n->aux_bool == 0) {
            _puts(ctx, "false");
        } else {
            _puts(ctx, "true");
        }
        break;

    case HNF_SCALE2:
    case HNF_SCALE3:
    case HNF_DIV:
        infix(n, "/", ctx);
        break;

    case HNF_SHIFT2:
        _putc(ctx, '(');
        co(n->inputs->node, ctx);
        _puts(ctx, "+(double2)(");
        co(n->inputs->next->node, ctx);
        _putc(ctx, ',');
        co(n->inputs->next->next->node, ctx);
        _puts(ctx, "))");

    case HNF_ADD:
        infix(n, "+", ctx);
        break;

    case HNF_SUB:
        infix(n, "-", ctx);
        break;

    case HNF_MUL:
        infix(n, "*", ctx);
        break;

    case HNF_ISEQ:
        infix(n, "==", ctx);
        break;

    case HNF_GT:
        infix(n, ">", ctx);
        break;

    case HNF_GTE:
        infix(n, ">=", ctx);
        break;

    case HNF_LT:
        infix(n, "<", ctx);
        break;

    case HNF_LTE:
        infix(n, "<=", ctx);
        break;

    case HNF_AND:
        infix(n, "&&", ctx);
        break;

    case HNF_OR:
        infix(n, "||", ctx);
        break;

    case HNF_X:
        co(n->inputs->node, ctx);
        _puts(ctx, ".x");
        break;

    case HNF_Y:
        co(n->inputs->node, ctx);
        _puts(ctx, ".y");
        break;

    case HNF_Z:
        co(n->inputs->node, ctx);
        _puts(ctx, ".z");
        break;

    case HNF_XY:
        co(n->inputs->node, ctx);
        _puts(ctx, ".xy");
        break;

    case HNF_NEG:
        _putc(ctx, '-');
        co(n->inputs->node, ctx);
        break;

    case HNF_NOT:
        _putc(ctx, '!');
        co(n->inputs->node, ctx);
        break;

    case HNF_MAP2:
        map_fn(n, ctx);
        break;

    case HNF_THEN_ELSE:
        _putc(ctx, '(');
        co(n->inputs->node, ctx);
        _puts(ctx, ")?(");
        co(n->inputs->next->node, ctx);
        _puts(ctx, "):(");
        co(n->inputs->next->next->node, ctx);
        _putc(ctx, ')');
        break;

    default:
        _puts(ctx, ">>> NOT IMPLEMENTED <<<");
    }
}

static void
pl(hxnoise_ast_node *n, context_t *ctx)
{
    hxnoise_ast_node_list *i = n->inputs;
    _putc(ctx, '(');
    while (i != NULL) {
        co(i->node, ctx);
        i = i->next;
        if (i != NULL) {
            _putc(ctx, ',');
        }
    }
    _putc(ctx, ')');
}

char *
hxnoise_compile(function_in_t *def, int count)
{
    context_t ctx;
    memset(&ctx, sizeof(context_t), 0);

    buffer_t *resbuf = buffer_new_with_string(strdup(HXOCL_HEADER));
    buffer_t *buf = buffer_new();

    ctx.count = 0;
    ctx.funcs = slist_new();

    for (int i = 0; i < count; i++) {
        ctx.buf = buffer_new();
        co(def[i].ast, &ctx);
        buffer_appendf(buf, HXOCL_KERNEL_3D_DOUBLE, def[i].name);
        buffer_appendb(buf, ctx.buf);
        buffer_append(buf, ";\n}\n");
        buffer_free(ctx.buf);
    }

    if (ctx.used_funcs[HNF_PERLIN2] == 1 || ctx.used_funcs[HNF_PERLIN3] == 1) {
        buffer_append(resbuf, HXOCL_LERP);
        buffer_append(resbuf, HXOCL_BLENDFUNCS);
        buffer_append(resbuf, HXOCL_PERLIN_GRADIENT);
    }

    for (int i = 0; i < HNF_NR_OF_FUNCTIONS; i++) {
        if (ctx.used_funcs[i] == 1) {
            const char *body = opencl_impl(i);
            if (body != NULL) {
                buffer_append(resbuf, body);
            }
        }
    }

    slist_node_t* iter = ctx.funcs->head;
    while (iter != NULL) {
        function_def_t *fn = iter->val;
        buffer_appendb(resbuf, fn->body);
        free(fn->name);
        buffer_free(fn->body);
        iter = iter->next;
    }

    buffer_appendb(resbuf, buf);
    buffer_free(buf);
    
    char *result = buffer_string(resbuf);
    resbuf->alloc = NULL;
    buffer_free(resbuf);

    return result;
}
