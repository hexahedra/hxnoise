
#ifndef HXNOISE_COMPILE_H
#define HXNOISE_COMPILE_H 1

#include <stdio.h>

#include "ast.h"

typedef enum {
    HNP_INT16,
    HNP_SINGLE,
    HNP_DOUBLE,
} hxnoise_precision_t;

typedef struct {
    char *name;
    hxnoise_ast_node *ast;
    hxnoise_precision_t precision;
} function_in_t;

char *
hxnoise_compile(function_in_t *def, int count);

#endif
