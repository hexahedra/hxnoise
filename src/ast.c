
#include "ast.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "function.h"
#include "generator_context.h"

typedef struct {
    char *name;
    hxnoise_var_t type;
    int is_optional;
    double default_value;
} param_def;

typedef struct {
    char *name;
    hxnoise_func_t id;
    hxnoise_var_t return_type;
    int is_const;
    hxnoise_var_t first_arg_type;
    int param_count;
    param_def *params;
} func_def;

static param_def p_angle[] = { { "angle", HNT_VAR, 0, 0.0 } };
static param_def p_div[] = { { "div", HNT_VAR, 0, 0.0 } };
static param_def p_xyz_s[] = { { "x", HNT_VAR, 0, 0.0 }, { "y", HNT_VAR, 0, 0.0 }, { "z", HNT_VAR, 0, 0.0 } };
static param_def p_seed[] = { { "seed", HNT_VAR, 1, 0.0 } };
static param_def p_filename[] = { { "filename", HNT_STRING, 0, 0.0 } };
static param_def p_zero[] = { { "v", HNT_VAR, 1, 0.0 } };
static param_def p_one[] = { { "v", HNT_VAR, 1, 1.0 } };
static param_def p_two[] = { { "v", HNT_VAR, 1, 2.0 } };

static func_def functions[] = {
    { "!input", HNN_INPUT,        HNT_XY,     0, HNT_NONE, 0, NULL },
    { "!var",   HNN_CONST_VAR,    HNT_VAR,    1, HNT_NONE, 0, NULL },
    { "!str",   HNN_CONST_STRING, HNT_STRING, 1, HNT_NONE, 0, NULL },
    { "!bool",  HNN_CONST_BOOL,   HNT_BOOL,   1, HNT_NONE, 0, NULL },

    { "rotate",         HNF_ROTATE2,        HNT_XY,     0, HNT_XY  , 1, p_angle },
    { "scale",          HNF_SCALE2,         HNT_XY,     0, HNT_XY  , 1, p_div   },
    { "shift",          HNF_SHIFT2,         HNT_XY,     0, HNT_XY  , 2, p_xyz_s },
    { "swap",           HNF_SWAP,           HNT_XY,     0, HNT_XY  , 0, NULL    },
    { "turbulence",     HNF_TURBULENCE2,    HNT_XY,     0, HNT_XY  , 2, p_xyz_s },
    { "map",            HNF_MAP2,           HNT_XY,     0, HNT_XY  , 2, p_xyz_s },
    { "angle",          HNF_ANGLE,          HNT_VAR,    0, HNT_XY  , 0, NULL    },
    { "chebyshev",      HNF_CHEBYSHEV2,     HNT_VAR,    0, HNT_XY  , 0, NULL    },
    { "distance",       HNF_DISTANCE2,      HNT_VAR,    0, HNT_XY  , 0, NULL    },
    { "manhattan",      HNF_MANHATTAN2,     HNT_VAR,    0, HNT_XY  , 0, NULL    },
    { "perlin",         HNF_PERLIN2,        HNT_VAR,    0, HNT_XY  , 1, p_seed  },
    { "png_lookup",     HNF_PNG_LOOKUP,     HNT_VAR,    0, HNT_XY  , 1, p_filename },
    { "simplex",        HNF_SIMPLEX2,       HNT_VAR,    0, HNT_XY  , 1, p_seed  },
    { "opensimplex",    HNF_OPENSIMPLEX2,   HNT_VAR,    0, HNT_XY  , 1, p_seed  },
    { "x",              HNF_X,              HNT_VAR,    0, HNT_XY  , 0, NULL    },
    { "y",              HNF_Y,              HNT_VAR,    0, HNT_XY  , 0, NULL    },

    { "add",            HNF_ADD,            HNT_VAR,    0, HNT_VAR , 1, p_one   },
    { "sub",            HNF_SUB,            HNT_VAR,    0, HNT_VAR , 1, p_one   },
    { "mul",            HNF_MUL,            HNT_VAR,    0, HNT_VAR , 1, p_two   },
    { "div",            HNF_DIV,            HNT_VAR,    0, HNT_VAR , 1, p_two   },
    { "abs",            HNF_ABS,            HNT_VAR,    0, HNT_VAR , 0, NULL    },
    { "sin",            HNF_SIN,            HNT_VAR,    0, HNT_VAR , 0, NULL    },
    { "cos",            HNF_COS,            HNT_VAR,    0, HNT_VAR , 0, NULL    },
    { "max",            HNF_MAX,            HNT_VAR,    0, HNT_VAR , 1, p_zero  },
    { "min",            HNF_MIN,            HNT_VAR,    0, HNT_VAR , 1, p_zero  },
    { "neg",            HNF_NEG,            HNT_VAR,    0, HNT_VAR , 0, NULL    },
    { "pow",            HNF_POW,            HNT_VAR,    0, HNT_VAR , 1, p_two   },
    { "round",          HNF_ROUND,          HNT_VAR,    0, HNT_VAR , 0, NULL    },

    { NULL,             HNN_INPUT,          HNT_NONE,   0, HNT_NONE, 0, NULL    }
};

/* ------------------------------------------------------------------------- */

static char *string_error(const char *fmt, const char *arg)
{
    char *result;
    int needed = snprintf(NULL, 0, fmt, arg) + 1;
    result = malloc(needed);
    snprintf(result, needed, fmt, arg);
    return result;
}

static char *int_error(const char *fmt, int arg)
{
    char *result;
    int needed = snprintf(NULL, 0, fmt, arg) + 1;
    result = malloc(needed);
    snprintf(result, needed, fmt, arg);
    return result;
}

static func_def *
find_func(const char *name)
{
    func_def *fd = functions;
    while (fd->name != NULL) {
        if (!strcmp(name, fd->name)) {
            break;
        }
        ++fd;
    }

    return fd->name != NULL ? fd : NULL;
}

static hxnoise_ast_node_list *
make_node(hxnoise_function *func, hxnoise_generator_context *ctx)
{
    hxnoise_ast_node_list *res = malloc(sizeof(hxnoise_ast_node_list));

    res->node = hxnoise_build_ast(func, ctx);
    res->next = NULL;

    return res;
}

static void
make_const_var_node(hxnoise_ast_node *result, double value)
{
    memset(result, 0, sizeof(hxnoise_ast_node));
    result->type = HNN_CONST_VAR;
    result->return_type = HNT_VAR;
    result->is_const = 1;
    result->aux_var = value;
}

static hxnoise_ast_node_list *
make_const_var_node_list(double value)
{
    hxnoise_ast_node_list *res = malloc(sizeof(hxnoise_ast_node_list));
    res->node = malloc(sizeof(hxnoise_ast_node));
    make_const_var_node(res->node, value);
    res->next = NULL;

    return res;
}

static void
handle_func(hxnoise_ast_node *nd, hxnoise_function *func, hxnoise_generator_context *ctx)
{
    assert(nd != NULL);
    assert(func != NULL);
    assert(func->name != NULL);

    func_def *fd = find_func(func->name);
    if (fd == NULL) {
        nd->type = HNN_ERROR;
        nd->err_msg = string_error("Unknown function '%s'", func->name);
        return;
    }

    nd->type        = fd->id;
    nd->return_type = fd->return_type;
    nd->is_const    = fd->is_const;
    nd->inputs      = make_node(func->input, ctx);

    hxnoise_var_t ntr = nd->inputs->node->return_type;
    if (ntr != HNT_NONE && ntr != fd->first_arg_type) {
        nd->err_msg = int_error("First argument type mismatch '%i'", ntr);
        return;
    }

    hxnoise_ast_node_list *insert = nd->inputs;
    hxnoise_function_list *arg    = func->args;
    int                    count  = 0;

    while (arg != NULL) {
        if (++count > fd->param_count) {
            nd->err_msg = int_error("Too many arguments (should be %i)", fd->param_count);
        }

        insert->next = make_node(arg->elem, ctx);
        insert = insert->next;
        arg = arg->next;
    }

    while (count < fd->param_count) {
        param_def *pd = &fd->params[count];
        if (pd->is_optional == 1) {
            insert->next = make_const_var_node_list(pd->default_value);
            insert = insert->next;
        } else if (nd->err_msg == NULL) {
            nd->err_msg = string_error("Parameter '%s' is not optional", pd->name);
        }

        ++count;
    }
}


hxnoise_ast_node *
hxnoise_build_ast(hxnoise_function *func, hxnoise_generator_context *ctx)
{
    hxnoise_ast_node *result = malloc(sizeof(hxnoise_ast_node));
    memset(result, 0, sizeof(hxnoise_ast_node));

    if (func == NULL) {
        result->type = HNN_INPUT;
        return result;
    }

    switch (func->type) {
    case HN_FUNC:
        handle_func(result, func, ctx);
        break;

    case HN_CONST_VALUE:
        make_const_var_node(result, func->value);
        break;

    case HN_CONST_STRING:
        result->type = HNN_CONST_STRING;
        result->return_type = HNT_STRING;
        result->is_const = 1;
        result->aux_string = func->name;
        break;

    case HN_CONST_BOOL:
        result->type = HNN_CONST_BOOL;
        result->return_type = HNT_BOOL;
        result->is_const = 1;
        result->aux_var = func->value > 0;
        break;

    default:
        result->type = HNN_ERROR;
        result->aux_string = int_error("Unknown type #%i", func->type);
    }

    return result;
}

void
hxnoise_ast_destroy(hxnoise_ast_node *node)
{
    hxnoise_ast_node_list *i = node->inputs;
    while (i != NULL) {
        hxnoise_ast_node_list *f = i;
        hxnoise_ast_destroy(i->node);
        i = i->next;
        free(f);
    }

    free(node->err_msg);
    free(node->aux_string);
    free(node);
}

const char *
hxnoise_func_name(hxnoise_func_t id)
{
    if (id == HNN_ERROR) {
        return "error";
    }

    func_def *fd = functions;
    while (fd->name != NULL) {
        if (fd->id == id) {
            break;
        }
        ++fd;
    }

    return fd->name;
}