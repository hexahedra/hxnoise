HexaNoise
=========

This is a library for generating procedural 2-D and 3-D noise. It takes
a compact description of the noise function as input, and compiles it to
OpenCL.

Although it was originally developed for generating blocky voxel worlds,
it can also be used for procedural textures, heightmaps, and other assets.

![Terrain generated from noise functions](http://imgur.com/Px3TqoX.jpg)

Examples
--------

Let's start with trusty ol' Perlin. 1 pixel is 1 unit, so we first scale it by
a factor 50 so we can see some detail.

    scale(50):perlin

![scale(50):perlin](http://imgur.com/QrS4lIo.png)

Here we take the absolute value of the output of [OpenSimplex](https://en.wikipedia.org/wiki/OpenSimplex_noise) to turn it into billow noise. 
This function is then passed as a parameter to `fractal`, which uses it
to generate a number of octaves of fractal noise.

    scale(20):fractal(opensimplex:abs)

![scale(20):fractal(opensimplex:abs)](http://imgur.com/ZDvzD62.png)

A more complex example that blends two noise functions together using a
checkerboard pattern.

    scale(250):checkerboard:blend
    (
        scale(10):distance:sin,
        scale(100):fractal(perlin,7)
    )


![scale(250):checkerboard:blend(scale(10):distance:sin,scale(100):fractal(perlin,7)](http://imgur.com/XkB5FPO.png)

Try it online
-------------

[Experiment with HNDL](https://hippie.nu/~nocte/hndltest) in your browser. You can
also find more examples there.

Documentation
-------------

(TBD)

License
-------

The HexaNoise Library is free software: you can distribute it and/or modify
it under the terms of the [MIT license](http://opensource.org/licenses/MIT).
